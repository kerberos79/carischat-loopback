'use strict';

module.exports = function(Accident) {

    Accident.remoteMethod(
        'roomkey',
        {
            accepts: { arg: 'roomkey', type: 'string', require: true },
            http: { path: '/roomkey/:roomkey', verb: 'get'},
            returns: { type: 'accident', root: true}
        }
    )

    Accident.roomkey = function (roomkey, cb) {
        Accident.findOne( { where: {roomkey: roomkey}}, function (err, accident) {
            cb && cb(err, accident);
        });
    };
};
