'use strict';

import loopback from 'loopback';
import boot from 'loopback-boot';
import config from '../app.config';

const app = module.exports = loopback();

function createTables(app) {
    // const models = ['accident', 'caristest'];
    const models = ['caristest'];
    const ds = app.datasources['postgres'];
    ds.isActual(models, (e, actual) => {
        if (!actual) {
            ds.autoupdate(models, function (err) {
                console.log(err);
            });
        }
    });
}


if (process.env.NODE_ENV === 'development') {

    const webpack = require('webpack');
    const WebpackDevServer = require('webpack-dev-server');

    app.start = function () {
        return app.listen(function () {
            app.emit('started');
            const baseUrl = app.get('url').replace(/\/$/, '');
            console.log('Web server listening at: %s', baseUrl);
            if (app.get('loopback-component-explorer')) {
                var explorerPath = app.get('loopback-component-explorer').mountPath;
                console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
            }
        });
    };

    boot(app, __dirname, function (err) {
        if (err)
            throw err;

        if (require.main === module) {
            app.start();

            const webpackConfig = require('../webpack.dev.config');
            const compiler = webpack(webpackConfig);
            const devServer = new WebpackDevServer(compiler, webpackConfig.devServer);
            devServer.listen(config.server.devPort, () => {
                console.log('## webpack-dev-server is listening on port', config.server.devPort);
            });
        }
    });

} else if (process.env.NODE_ENV === 'production') {

    app.start = function () {
        return app.listen(function () {
            app.emit('started');
            const baseUrl = app.get('url').replace(/\/$/, '');
            console.log('Web server listening at: %s', baseUrl);
            if (app.get('loopback-component-explorer')) {
                var explorerPath = app.get('loopback-component-explorer').mountPath;
                console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
            }
        });
    };

    boot(app, __dirname, function (err) {
        if (err)
            throw err;

        if (require.main === module) {
            app.start();
        }
    });
}



