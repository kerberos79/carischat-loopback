var gulp = require('gulp'),
    gulpsync = require('gulp-sync')(gulp),
    gutil = require('gulp-util'),
    uglify  = require('gulp-uglify'),
    concat  = require('gulp-concat'),
    babel = require('gulp-babel'),
    webpack = require('webpack-stream'),
    webpackConfig = require('./webpack.config.js'),
    react = require('gulp-react'),
    clean = require('gulp-clean'),
    zip = require('gulp-zip'),
    jsonfile = require('jsonfile');


const package = require('./package.json');
const DEST_TMP = './dist/tmp/';

gulp.task('before', function() {
    return gulp.src('./dist')
                .pipe(clean());
});

gulp.task('build-server', function() {
    return gulp.src('server/carischat_server.js')
                .pipe(babel({
                    presets: ["es2015"]
                }))
                .pipe(uglify())
                .pipe(concat('carischat_server.js'))
                .pipe(gulp.dest(DEST_TMP + 'bin/'));
});

gulp.task('copy-server-boot', function() {
    return gulp.src('server/boot/**/*')
                .pipe(gulp.dest(DEST_TMP + 'bin/boot'));
});

gulp.task('copy-server-config', function() {
    return gulp.src('server/*.json')
                .pipe(gulp.dest(DEST_TMP + 'bin/'));
});

gulp.task('copy-lb-models', function() {
    return gulp.src('./common/**/*')
                .pipe(gulp.dest(DEST_TMP + 'common/'));
});

gulp.task('copy-app-config', function() {

    const newPackage = package;
    delete newPackage.devDependencies;
    delete newPackage.repository;
    delete newPackage.scripts.dev;
    delete newPackage.scripts.build;
    const filepath = DEST_TMP + 'package.json';
    jsonfile.writeFileSync(filepath, newPackage, {spaces: 4, EOL: '\r\n'});

    return gulp.src(['app.config.js', 'pm2.json'])
                .pipe(gulp.dest(DEST_TMP));
});

gulp.task('copy-browser-config', function() {
    return gulp.src('./public/**')
                .pipe(gulp.dest(DEST_TMP + '/public/'));
});

gulp.task('build-browser', function() {
    return gulp.src('./browser/index.js')
                .pipe(babel({
                    presets: ["es2015", "stage-0", "react"]
                }))
                .pipe(webpack(webpackConfig))
                .pipe(gulp.dest(DEST_TMP + '/public/'));
});

gulp.task('zip', function() {
    const destFilename = 'caris_ver_' + package.version + '.zip';
    return gulp.src(DEST_TMP + '**')
                .pipe(zip(destFilename))
                .pipe(gulp.dest('dist'))
});

gulp.task('after', function() {
    return gulp.src(DEST_TMP)
                .pipe(clean());
});

gulp.task('default', gulpsync.sync([
                        'before',
                        'build-server',
                        'copy-server-boot',
                        'copy-server-config',
                        'copy-lb-models',
                        'copy-app-config',
                        'copy-browser-config',
                        'build-browser',
                        'zip',
                        'after'
                        ]));







