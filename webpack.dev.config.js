var config = require('./app.config');
var webpack = require('webpack');

module.exports = {
    devtool: 'eval-source-map',
    entry: [
        'babel-polyfill',
        './browser/index.js',
        'webpack-dev-server/client?http://0.0.0.0:' + config.server.devPort,
        'webpack/hot/only-dev-server'
    ],
    output: {
        path: '/',
        filename: 'bundle.js'
    },
    devServer: {
        compress: true,
        disableHostCheck: true,
        hot: true,
        filename: 'bundle.js',
        publicPath: '/',
        historyApiFallback: true,
        contentBase: './public',
        proxy: {
            "**": "http://0.0.0.0:" + config.server.port
        }
    },

    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.ProvidePlugin({
            Promise: "bluebird"
        }),
    ],

    module: {
        loaders: [
            {
                test: /\.js$/,
                loaders: ['react-hot-loader', 'babel-loader?' + JSON.stringify({
                    cacheDirectory: true,
                    presets: ['es2015', 'stage-0', 'react']
                })],
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.json$/,
                loader: 'json-loader'
            }
        ]
    },

};
