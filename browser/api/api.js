import axios from 'axios';
import FormData from 'form-data';

const api = (() => {

    const api = {};

    api.getAccidentsByID = function (id, cb) {

      axios.get(`/api/accidents/${id}`).then((result) => {
          cb && cb(null, result.data);
      }).catch ((error) => {
          cb && cb(error);
      });
    };

    api.getAccidentsByRoomKey = function (roomkey, cb) {

        axios.get(`/api/accidents/roomkey/${roomkey}`).then((result) => {
            cb && cb(null, result.data);
        }).catch ((error) => {
            cb && cb(error);
        });
    };


    api.getAccidents = (cb) => {

        axios.get('/api/accidents').then((result) => {
            cb && cb(null, result.data);
        }).catch ((error) => {
            cb && cb(error);
        });
    };

    api.postAccident = (params, cb) => {

        axios.post('/api/accidents', params).then((result) => {
          cb && cb(null, result.data);
        }).catch ((error) => {
            cb && cb(error);
        });
    };

    api.updateAccident = (roomkey, params, cb) => {

        axios.post(`/api/accidents/update?where[roomkey]=${roomkey}`, params).then((result) => {
            cb && cb(null, result.data);
        }).catch ((error) => {
            cb && cb(error);
        });
    };

    api.postCreateContainer = (roomkey, cb) => {

        axios.post('/api/containers', {name:roomkey}).then((result) => {
            cb && cb(null, result.data);
        }).catch ((error) => {
            cb && cb(error);
        });
    };

    api.getContainer = (roomkey, cb) => {

        axios.get('/api/containers', {name:roomkey}).then((result) => {
            cb && cb(null, result.data);
        }).catch ((error) => {
            cb && cb(error);
        });
    };


    api.postFiles = (roomkey, files, cb) => {
        if (!roomkey) {
            cb &&  cb(new Error().message = "roomkey not exist");
            return;
        }

        if (!files) {
            cb &&  cb(new Error().message = "file not exist");
            return;
        }
        const form = new FormData();
        files.forEach((file) => {
            form.append('files', file);
        });
        axios.post(`/api/containers/${roomkey}/upload`, form).then((result) => {
            cb && cb(null, result.data);
        }).catch ((error) => {
            cb && cb(error);
        });
    };

    return api;

})();

export default api
