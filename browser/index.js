import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import View from './home/View';
import Register from './home/Register';

const rootElement = document.getElementById('app');
ReactDOM.render(
    <BrowserRouter>
        <Switch>
            <Route exact path='/register' component={Register}/>
            <Route exact path='/view' component={View}/>
        </Switch>
    </BrowserRouter>, rootElement);
