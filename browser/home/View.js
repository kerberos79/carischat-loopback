import React from 'react';
import axios from 'axios';
import queryString from 'query-string';
import api from '../api/api';

class View extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            date: '',
            type: '폭발',
            localtion: '',
            content: '',
            cause: '',
            materialKor: '',
            materialCAS: '',
            carisResult: '',				//HTML 형식
            humanDamage: '',
            propertyDamage: '',
            environmentalDamage: '',
            actionState: '',
            leakMethod: '',
            separationDistance: '',
            protectiveDistance: '',
            individualProtection: '',
        }
        this.articleInfoRequest = this.articleInfoRequest.bind(this);

    }

    componentDidMount() {
        this.articleInfoRequest();
    }

    articleInfoRequest = () => {
        var parsed = queryString.parse(this.props.location.search);
        console.log(parsed.roomkey);
        api.getAccidentsByRoomKey(parsed.roomkey, (e, json) => {
            if (e == null) {
                this.setState({
                    date: json.date,
                    type: json.type,
                    localtion: json.localtion,
                    content: json.content,
                    cause: json.cause,
                    materialKor: json.material_kor,
                    materialCAS: json.material_cas,
                    carisResult: json.caris_result,
                    humanDamage: json.human_damage,
                    propertyDamage: json.property_damage,
                    environmentalDamage: json.environmental_damage,
                    actionState: json.action_state,
                    leakMethod: json.leak_method,
                    separationDistance: json.separation_distance,
                    protectiveDistance: json.protective_distance,
                    individualProtection: json.individual_protection
                });
            }
            else {
                console.log(e);
            }
        });
    }

    render() {
        const styleLabel = {
            width: '100%',
            height: 40,
            background: '#dddddd',
            padding: 10
        }
        const styleContent = {
            width: '100%',
            padding: 10
        }
        return (
            <div style={{width: '100%'}}>
                <div>
                    <label style={styleLabel}>사고일시</label>
                    <div style={styleContent}>
                        {this.state.date}
                    </div>
                </div>

                <div>
                    <label style={styleLabel}>사고유형</label>
                    <div style={styleContent}>
                        {this.state.type}
                    </div>
                </div>

                <div>
                    <label style={styleLabel}>사고장소</label>
                    <div style={styleContent}>
                        {this.state.localtion}
                    </div>
                </div>

                <div>
                    <label style={styleLabel}>사고내용</label>
                    <div style={styleContent}>
                        {this.state.content}
                    </div>
                </div>

                <div>
                    <label style={styleLabel}>사고원인</label>
                    <div style={styleContent}>
                        {this.state.cause}
                    </div>
                </div>
                <div>
                    <label style={styleLabel}>사고물질(국)</label>
                    <div style={styleContent}>
                        {this.state.materialKor}
                    </div>
                </div>

                <div>
                    <label style={styleLabel}>사고물질(CAS)</label>
                    <div style={styleContent}>
                        {this.state.materialCAS}
                    </div>
                </div>

                <div>
                    <label style={styleLabel}>CARIS 구동결과</label>
                    <div style={styleContent} dangerouslySetInnerHTML={{__html: this.state.carisResult}}>
                    </div>
                </div>

                <div>
                    <label style={styleLabel}>인명피해</label>
                    <div style={styleContent}>
                        {this.state.humanDamage}
                    </div>
                </div>


                <div>
                    <label style={styleLabel}>재산피해</label>
                    <div style={styleContent}>
                        {this.state.propertyDamage}
                    </div>
                </div>

                <div>
                    <label style={styleLabel}>환경피해</label>
                    <div style={styleContent}>
                        {this.state.environmentalDamage}
                    </div>
                </div>

                <div>
                    <label style={styleLabel}>조치상황</label>
                    <div style={styleContent}>
                        {this.state.actionState}
                    </div>
                </div>

                <div>
                    <label style={styleLabel}>누출방제요령</label>
                    <div style={styleContent}>
                        {this.state.leakMethod}
                    </div>
                </div>

                <div>
                    <label style={styleLabel}>조기이격거리</label>
                    <div style={styleContent}>
                        {this.state.separationDistance}
                    </div>
                </div>

                <div>
                    <label style={styleLabel}>방호활동거리</label>
                    <div style={styleContent}>
                        {this.state.protectiveDistance}
                    </div>
                </div>

                <div>
                    <label style={styleLabel}>노출방지 및 개인보호구</label>
                    <div style={styleContent}>
                        {this.state.individualProtection}
                    </div>
                </div>
            </div>
        )
    }
}

export default View
