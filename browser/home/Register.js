import React, {Component} from 'react';
import Center from 'react-center';
import {EditorState, convertToRaw, ContentState} from 'draft-js';
import {Editor} from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import {Button, FormGroup, FormControl, ControlLabel} from 'react-bootstrap';
import {ButtonGroup, ButtonToolbar, ToggleButtonGroup, ToggleButton, DropdownButton, MenuItem} from 'react-bootstrap';
import {Grid, Row, Col} from 'react-bootstrap';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Modal} from 'react-bootstrap';
import axios from 'axios';
import queryString from 'query-string';
import api from '../api/api';

class Register extends Component {
    constructor(props) {
        super(props);

        var startDate = new Date();
        this.state = {
            editorState: EditorState.createEmpty(),
            uploadFiles:[],            
            uploadedImages: [],
            date: '',
            type: '폭발',
            location: '',
            content: '',
            cause: '',
            materialKor: '',
            materialCAS: '',
            carisResult: '',
            humanDamage: '',
            propertyDamage: '',
            environmentalDamage: '',
            actionState: '',
            leakMethod: '',
            separationDistance: '',
            protectiveDistance: '',
            individualDistance: '',
            alreadyExist: false,
            stateModal: false,
            contentModal: ''
        }
        this.handleChangeDate = this.handleChangeDate.bind(this);
        this.handleChangeLocation = this.handleChangeLocation.bind(this);
        this.handleChangeContent = this.handleChangeContent.bind(this);
        this.handleChangeCause = this.handleChangeCause.bind(this);
        this.handleChangeMaterialKor = this.handleChangeMaterialKor.bind(this);
        this.handleChangeMaterialCAS = this.handleChangeMaterialCAS.bind(this);
        this.handleChangeCarisResult = this.handleChangeCarisResult.bind(this);
        this.handleChangeHumanDamage = this.handleChangeHumanDamage.bind(this);
        this.handleChangePropertyDamage = this.handleChangePropertyDamage.bind(this);
        this.handleChangeEnvironmentalDamage = this.handleChangeEnvironmentalDamage.bind(this);
        this.handleChangeActionState = this.handleChangeActionState.bind(this);
        this.handleChangeLeakMethod = this.handleChangeLeakMethod.bind(this);
        this.handleChangeSeparationDistance = this.handleChangeSeparationDistance.bind(this);
        this.handleChangeProtectiveDistance = this.handleChangeProtectiveDistance.bind(this);
        this.handleChangeIndividualDistance = this.handleChangeIndividualDistance.bind(this);
        this.onEditorStateChange = this.onEditorStateChange.bind(this);
        this.uploadCallback = this.uploadCallback.bind(this);
        this.onPostForm = this.onPostForm.bind(this);        
        this.onContentStateChange = this.onContentStateChange.bind(this);
        this.handleChangeDate = this.handleChangeDate.bind(this);

        this.articleInfoRequest = this.articleInfoRequest.bind(this);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);

    }

    openModal = (content) => {
        this.setState({
            stateModal: true,
            contentModal: content
        });
    }
    closeModal = () => {
        this.setState({
            stateModal: false
        });
    }

    componentDidMount() {
        this.articleInfoRequest();
    }

    uploadCallback = (file) => {
        let uploadedImages = this.state.uploadedImages;

        const imageObject = {
            file: file,
            localSrc: URL.createObjectURL(file),
        }
        uploadedImages.push(imageObject);
        this.setState({uploadedImages: uploadedImages});
        return new Promise(
            (resolve, reject) => {
                resolve({data: {link: imageObject.localSrc}});
            }
        );
    };

    handleChangeDate = (e) => {
        this.setState({date: e.target.value});
    }
    handleChangeLocation = (e) => {
        this.setState({location: e.target.value});
    }
    handleChangeContent = (e) => {
        this.setState({content: e.target.value});
    }
    handleChangeCause = (e) => {
        this.setState({cause: e.target.value});
    }
    handleChangeMaterialKor = (e) => {
        this.setState({materialKor: e.target.value});
    }
    handleChangeMaterialCAS = (e) => {
        this.setState({materialCAS: e.target.value});
    }
    handleChangeCarisResult = (e) => {
        this.setState({carisResult: e.target.value});
    }


    handleChangeHumanDamage = (e) => {
        this.setState({humanDamage: e.target.value});
    }
    handleChangePropertyDamage = (e) => {
        this.setState({propertyDamage: e.target.value});
    }
    handleChangeEnvironmentalDamage = (e) => {
        this.setState({environmentalDamage: e.target.value});
    }
    handleChangeActionState = (e) => {
        this.setState({actionState: e.target.value});
    }
    handleChangeLeakMethod = (e) => {
        this.setState({leakMethod: e.target.value});
    }
    handleChangeSeparationDistance = (e) => {
        this.setState({separationDistance: e.target.value});
    }
    handleChangeProtectiveDistance = (e) => {
        this.setState({protectiveDistance: e.target.value});
    }
    handleChangeLeakMethod = (e) => {
        this.setState({leakMethod: e.target.value});
    }
    handleChangeIndividualDistance = (e) => {
        this.setState({individualDistance: e.target.value});
    }
    onEditorStateChange = (editorState1) => {
        this.setState({
            editorState: editorState1,
        });
    };

    uploadCallback = (file) => {
        let uploadFiles = this.state.uploadFiles;        
        let uploadedImages = this.state.uploadedImages;

        const imageObject = {
            file: file,
            localSrc: URL.createObjectURL(file),
        }
        uploadedImages.push(imageObject);
        uploadFiles.push(file);
        this.setState({
            uploadFiles:uploadFiles,
            uploadedImages: uploadedImages
        });
        return new Promise(
            (resolve, reject) => {
                resolve({data: {link: imageObject.localSrc}});
            }
        );
    };
    uploadFile = (roomkey) => {
        let uploadFiles = this.state.uploadFiles;
        api.postCreateContainer(roomkey, (e, r) => {
          if(e==null) {
            api.postFiles(roomkey, uploadFiles, (e, r) => {
              if(e==null) {
                this.onPostForm(roomkey);
                console.log('postFiles '+r);
              }
              else {
                this.openModal("전송이 실패하였습니다.");
                console.log('postFiles '+e);
              }
            });
          }
          else {
            this.openModal("전송이 실패하였습니다.");
            console.log('postCreateContainer '+e);
          }
        });
      }
    
    onContentStateChange = (carisResult) => {
        this.setState({
            carisResult,
        });
    };  
    onPostForm = (roomkey) => {

        var htmlString = draftToHtml(this.state.carisResult);
        let uploadedImages = this.state.uploadedImages;
        for(var i=0;i<uploadedImages.length;i++) {
          htmlString = htmlString.replace(uploadedImages[i].localSrc, './resources/'+roomkey+'/'+uploadedImages[i].file.name);
        }
        console.log(htmlString);
        let data = {
            "roomkey": roomkey,
            "date": this.state.date,
            "type": this.state.type,
            "location": this.state.location,
            "content": this.state.content,
            "cause": this.state.cause,
            "material_kor": this.state.materialKor,
            "material_cas": this.state.materialCAS,
            "caris_result": htmlString,
            "human_damage": this.state.humanDamage,
            "property_damage": this.state.propertyDamage,
            "environmental_damage": this.state.environmentalDamage,
            "action_state": this.state.actionState,
            "leak_method": this.state.leakMethod,
            "separation_distance": this.state.separationDistance,
            "protective_distance": this.state.protectiveDistance,
            "individual_protection": this.state.individualProtection
        };

        if (this.state.alreadyExist) {
            api.updateAccident(roomkey, data, (e, r) => {
                if (e == null) {
                    this.openModal("전송이 완료되었습니다.");
                }
                else {
                    this.openModal("전송이 실패하였습니다.");
                }
                console.log(e);
                console.log(r);
            });
        }
        else {
            api.postAccident(data, (e, r) => {
                if (e == null) {
                    this.openModal("전송이 완료되었습니다.");
                }
                else {
                    this.openModal("전송이 실패하였습니다.");
                }
                console.log(e);
                console.log(r);
            });
        }
    }
    onSubmit = (event) => {
        //this.openProgress();
        var parsed = queryString.parse(this.props.location.search);
        console.log(parsed.roomkey);
        if(this.state.uploadFiles.length>0) {
          this.uploadFile(parsed.roomkey);
        }
        else {
          this.onPostForm(parsed.roomkey);
        }
      }
    
    articleInfoRequest = () => {
        var parsed = queryString.parse(this.props.location.search);
        console.log(parsed.roomkey);

        api.getAccidentsByRoomKey(parsed.roomkey, (e, json) => {
            if (e == null) {
                this.setState({
                    date: json.date,
                    type: json.type,
                    localtion: json.localtion,
                    content: json.content,
                    cause: json.cause,
                    materialKor: json.material_kor,
                    materialCAS: json.material_cas,
                    carisResult: json.caris_result,
                    humanDamage: json.human_damage,
                    propertyDamage: json.property_damage,
                    environmentalDamage: json.environmental_damage,
                    actionState: json.action_state,
                    leakMethod: json.leak_method,
                    separationDistance: json.separation_distance,
                    protectiveDistance: json.protective_distance,
                    individualProtection: json.individual_protection,
                    alreadyExist: true
                });
            }
            else {
                console.log(e);
            }
        });
    }

    render() {
        const editorStyle = {
            border: '1px solid grey',
            padding: '5px',
            borderRadius: '2px',
            height: '200px',
            width: '100%',
        };
        const styleFormGroup = {
            width: '100%',
            padding: '0px 10px 0px 10px'
        }
        const styleTitle = {
            width: '100%',
            height: 40,
            background: '#dddddd',
            padding: 10
        }
        const styleLabel = {
            width: '100%',
        }
        const styleContent = {
            width: '100%',
        }
        const styleTextArea = {
            width: '100%',
            height: 80
        }
        const {editorState} = this.state;
        return (
            <div
                style={{width: '100%'}}>
                <label style={styleTitle}>사고정보</label>
                <FormGroup
                    style={styleFormGroup}
                    controlId="formBasicText"
                >
                    <ControlLabel>사고일시</ControlLabel>
                    <FormControl
                        type="text"
                        style={styleContent}
                        value={this.state.date}
                        onChange={this.handleChangeDate}
                    />
                </FormGroup>
                <FormGroup
                    style={styleFormGroup}
                    controlId="formBasicText"
                >
                    <ControlLabel
                        style={styleLabel}>사고유형</ControlLabel>
                    <DropdownButton
                        title={this.state.type}
                        onSelect={this.onSelectType} id="bg-vertical-dropdown-1">
                        <MenuItem eventKey="폭발1">폭발1</MenuItem>
                        <MenuItem eventKey="폭발2">폭발2</MenuItem>
                        <MenuItem eventKey="폭발3">폭발3</MenuItem>
                    </DropdownButton>
                </FormGroup>

                <FormGroup
                    style={styleFormGroup}
                    controlId="formBasicText"
                >
                    <ControlLabel
                        style={styleLabel}>사고장소</ControlLabel>
                    <FormControl
                        type="text"
                        style={styleContent}
                        value={this.state.localtion}
                        onChange={this.handleChangeLocation}
                    />
                </FormGroup>

                <FormGroup
                    style={styleFormGroup}
                    controlId="formControlsTextarea"
                >
                    <ControlLabel
                        style={styleLabel}>사고내용</ControlLabel>
                    <FormControl
                        style={styleTextArea}
                        componentClass="textarea"
                        value={this.state.content}
                        onChange={this.handleChangeContent}
                    />
                </FormGroup>

                <FormGroup
                    style={styleFormGroup}
                    controlId="formBasicText"
                >
                    <ControlLabel
                        style={styleLabel}>사고물질(국)</ControlLabel>
                    <FormControl
                        type="text"
                        style={styleContent}
                        value={this.state.materialKor}
                        onChange={this.handleChangeMaterialKor}
                    />
                </FormGroup>

                <FormGroup
                    style={styleFormGroup}
                    controlId="formBasicText"
                >
                    <ControlLabel
                        style={styleLabel}>사고물질(CAS)</ControlLabel>
                    <FormControl
                        type="text"
                        style={styleContent}
                        value={this.state.materialCAS}
                        onChange={this.handleChangeMaterialCAS}
                    />
                </FormGroup>

                <FormGroup
                    style={styleFormGroup}
                    controlId="formBasicText"
                >
                    <ControlLabel
                        style={styleLabel}>CARIS 구동결과</ControlLabel>
                    <div
                        style={styleContent}>
                        <Editor
                            wrapperClassName="demo-wrapper"
                            editorClassName="demo-editor"
                            editorStyle={editorStyle}
                            toolbar={{image: {uploadCallback: this.uploadCallback}}}
                            onContentStateChange={this.onContentStateChange}
                        />
                    </div>
                </FormGroup>

                <FormGroup
                    style={styleFormGroup}
                    controlId="formBasicText"
                >
                    <ControlLabel
                        style={styleLabel}>인명피해</ControlLabel>
                    <FormControl
                        type="text"
                        style={styleContent}
                        value={this.state.humanDamage}
                        onChange={this.handleChangeHumanDamage}
                    />
                </FormGroup>
                <FormGroup
                    style={styleFormGroup}
                    controlId="formBasicText"
                >
                    <ControlLabel
                        style={styleLabel}>재산피해</ControlLabel>
                    <FormControl
                        type="text"
                        style={styleContent}
                        value={this.state.propertyDamage}
                        onChange={this.handleChangePropertyDamage}
                    />
                </FormGroup>

                <FormGroup
                    style={styleFormGroup}
                    controlId="formControlsTextarea"
                >
                    <ControlLabel
                        style={styleLabel}>환경피해</ControlLabel>
                    <FormControl
                        style={styleTextArea}
                        componentClass="textarea"
                        value={this.state.environmentalDamage}
                        onChange={this.handleChangeEnvironmentalDamage}
                    />
                </FormGroup>

                <FormGroup
                    style={styleFormGroup}
                    controlId="formControlsTextarea"
                >
                    <ControlLabel
                        style={styleLabel}>조치상황</ControlLabel>
                    <FormControl
                        style={styleTextArea}
                        componentClass="textarea"
                        value={this.state.actionState}
                        onChange={this.handleChangeActionState}
                    />
                </FormGroup>

                <FormGroup
                    style={styleFormGroup}
                    controlId="formControlsTextarea"
                >
                    <ControlLabel
                        style={styleLabel}>누출방제요령</ControlLabel>
                    <FormControl
                        style={styleTextArea}
                        componentClass="textarea"
                        value={this.state.leakMethod}
                        onChange={this.handleChangeLeakMethod}
                    />
                </FormGroup>
                <FormGroup
                    style={styleFormGroup}
                    controlId="formBasicText"
                >
                    <ControlLabel
                        style={styleLabel}>조기이격거리</ControlLabel>
                    <FormControl
                        type="text"
                        style={styleContent}
                        value={this.state.separationDistance}
                        onChange={this.handleChangeSeparationDistance}
                    />
                </FormGroup>
                <FormGroup
                    style={styleFormGroup}
                    controlId="formBasicText"
                >
                    <ControlLabel
                        style={styleLabel}>방호활동거리</ControlLabel>
                    <FormControl
                        type="text"
                        style={styleContent}
                        value={this.state.protectiveDistance}
                        onChange={this.handleChangeProtectiveDistance}
                    />
                </FormGroup>
                <FormGroup
                    style={styleFormGroup}
                    controlId="formControlsTextarea"
                >
                    <ControlLabel
                        style={styleLabel}>누출방지 및 개인보호구</ControlLabel>
                    <FormControl
                        style={styleTextArea}
                        componentClass="textarea"
                        value={this.state.individualDistance}
                        onChange={this.handleChangeIndividualDistance}
                    />
                </FormGroup>
                <Center>
                    <Button
                        style={{margin: '0px 0px 20px 0px'}}
                        bsStyle="primary"
                        onClick={this.onSubmit}>제출</Button>
                </Center>

                <Modal
                    show={this.state.stateModal}
                    onHide={this.closeModal}>
                    <Modal.Body>
                        {this.state.contentModal}
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.closeModal}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default Register;
