# README #

CarisChat
loopback + React.JS

## 설치 및 실행 방법 
#### 1. postgresql 테이블 생성
- common/models/accident.json 스키마 참고
- 테이블 생성 후 기본의로 생성되는 sampleid -> id 로 변경.

#### 2. 비트버킷에서 cariscahtloopback release 버전 다운로드

#### 3. gulp, pm2 전역설치(gulp:빌드, pm2:프로세스 관리)
- npm install gulp pm2

#### 4. Node.js 의존성 설치.
- npm install

#### 5. 빌드. (dist 디렉토리에 zip 파일로 바이너리 생성)
- npm run build

#### 6. 실행. (zip제외 모든 프로젝트 삭제 후 적당한 위치에 압축해제. 프로젝트 파일은 소스 파일이므로 반드시 삭제)
- a) 포트 변경 : app.config.js
- b) 빌드 버전 의존성 설치 : npm install
- c) 실행 : npm run start
- d) 중지 : npm run stop
- e) 프로세스 확인 : pm2 list
- f) 모니터링 : pm2 monit [id|name]
- z) 에러 로그 위치 확인 : pm2 show [id|name]

#### 7. 실행확인
- http://host:3000/status
- http://host:3000/view
- http://host:3000/register







